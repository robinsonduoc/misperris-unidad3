from django.db import models
from Apps.Usuario.models import Usuario
# Create your models here.

class Mascota(models.Model):
    nombre = models.CharField(max_length = 50)
    raza = models.CharField(max_length = 50)
    foto = models.ImageField(upload_to='img_reg_mas')
    descripcion = models.CharField(max_length = 100)
    estado_perro = (
        ('Disponible', 'Disponible'),
        ('Rescatado', 'Rescatado'),
        ('Adoptado', 'Adoptado')
    )
    estado = models.CharField(max_length = 20, choices = estado_perro)
    usuario = models.ForeignKey(Usuario, null = True, blank = True, on_delete = models.CASCADE)

    def __str__(self):
        return self.nombre
    