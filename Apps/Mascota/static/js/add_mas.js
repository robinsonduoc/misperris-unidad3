function validatexto(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[a-zA-ZÀ-ÿ\u00f1\u00d1]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}
