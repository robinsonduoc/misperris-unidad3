from django import forms
from .models import Mascota

class Mascota_form(forms.ModelForm):
    class Meta:
        model = Mascota
        fields = ['nombre', 'raza', 'foto', 'descripcion', 'estado',]
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'input-100'}),
            'raza': forms.TextInput(attrs={'class':'input-50'}),
            'foto': forms.FileInput(attrs={'class':'input-50'}),
            'descripcion': forms.TextInput(attrs={'class':'input-100'}),
            'estado': forms.Select(attrs={'class':'input-50'}),
        }