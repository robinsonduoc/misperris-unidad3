from django.conf.urls import url, include
from Apps.Mascota.views import add_mas, list_mas, upd_mas, del_mas, list_adop_mas, adop_mas

app_name = "Mascota"

urlpatterns = [
    url(r'^Formulario_mascota$', add_mas, name='Formulario_mascota'),
    url(r'^Listado_mascota$', list_mas, name='Listado_mascota'), 
    url(r'^Modificar_mascota/(?P<id_mascota>\d+)/$', upd_mas, name='Modificar_mascota'),
    url(r'^Eliminar_mascota/(?P<id_mascota>\d+)/$', del_mas, name='Eliminar_mascota'),
    url(r'^Listado_adopcion_mascota$', list_adop_mas, name='Listado_adopcion_mascota'),
]