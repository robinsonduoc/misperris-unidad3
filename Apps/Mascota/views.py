from django.shortcuts import render, redirect
#Importamos el formulario generado en forms.py
from .forms import Mascota_form
from .models import Mascota
#Importamos las apps
from Apps import Menu, Usuario
# Create your views here.

#llamamos el HTML de agregar mascotas
def index_add_mascota(request):
    return render(request, 'index_add_mas.html', {})

#Funcion que al ser un metodo post , se guarda la información ingresada en el formulario
def add_mas(request):
    if request.method == 'POST':
        form = Mascota_form(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('Menu:index',)
    else:
        form = Mascota_form()
        return render(request, 'index_add_mas.html', {'form':form})

#Funcion que me lista todas las mascotas ordenadas por nombre
def list_mas(request):
    mascota = Mascota.objects.all().order_by('nombre')
    return render(request, 'index_list_mas.html', {'perros':mascota})

#Funcion que edita los campos de las mascotas
def upd_mas(request, id_mascota):
    mascota = Mascota.objects.get(id = id_mascota)
    if request.method == 'GET':
        form = Mascota_form(instance = mascota)
    else:
        form = Mascota_form(request.POST, request.FILES, instance = mascota)
        if form.is_valid():
            mascota = form.save()
            mascota.save()
            return redirect('Mascota:Listado_mascota')
    return render(request, 'index_upd_mas.html', {'form':form})

#Funcion que elimina las mascotas
def del_mas(request, id_mascota):
    mascota = Mascota.objects.get(id = id_mascota)
    perro = Mascota.objects.all().order_by('nombre')
    if request.method == 'POST':
        mascota.delete()
        return redirect('Mascota:Listado_mascota')
    return render(request, 'index_del_mas.html', {'mascota':mascota})


def list_adop_mas(request):
    mascota = Mascota.objects.filter(estado = 'Disponible').order_by('nombre')
    return render(request, 'index_list_adop_mas.html', {'perros':mascota})

def adop_mas(request, id_mascota):
    mascota = Mascota.objects.get(id = id_mascota)
    mascota.estado = 'Adoptado'
    mascota.save()
    return redirect('Mascota:Listado_adopcion_mascota')