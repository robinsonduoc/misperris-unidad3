from django.conf.urls import url, include
from . import views

app_name = 'Usuario'

urlpatterns = [
    url(r'^Formulario_usuario/$', views.add_user, name='Formulario_usuario'),
]