from django import forms
from .models import Usuario

class Usuario_form(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('rut', 'nombre', 'telefono', 'correo', 'nacimiento', 'region', 'comuna', 'vivienda',)