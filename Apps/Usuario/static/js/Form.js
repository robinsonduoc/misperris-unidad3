$(function () {
    /*
        $.validator.setDefaults({
          errorClass:'error-label',
        highlight: function(element){
            $(element).addClass('error-control');
        },
        unhighlight: function(element){
        $(element).removeClass('error-control');
        }
      });
    */
    $("#mi-formulario").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            telefono: "required",
            pass2: {
                required: true,
                equalTo: "#telefono"
            }
        },
        messages: {
            email: {
                required: 'Ingresa tu correo electrónico',
                email: 'Formato de correo no válido'
            },
            telefono: {
                required: 'Ingresa una contraseña',
                minlength: 'Largo insuficiente'
            },
            pass2: {
                required: 'Reingresa la contraseña',
                equalTo: 'Las contraseñas ingresadas no coinciden',
                minlength: 'Largo insuficiente'

            }
        }
    });
});