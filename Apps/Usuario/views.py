from django.shortcuts import render, redirect
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from Apps.Usuario.serializers import UserSerializers, GroupSerializers


from .forms import Usuario_form
from .models import Usuario
from Apps import Menu, Mascota
 

# Create your views here.

#Vistas login y logout
def index_add_usuario(request):
    return render(request, 'index_add_us.html', {})

def index_login_user(request):
    return render(request, 'index_login_us.html', {})

def index_logout_user(request):
    return render(request, 'index_login_us.html', {})

#
def add_user(request):
    if request.method == 'POST':
        form = Usuario_form(request.POST)
        if form.is_valid():
            username = form.cleaned_data['nombre']
            password = form.cleaned_data['telefono']
            email = form.cleaned_data['correo']
            usuario = User.objects.create_user(username=username, email=email, password=password)
            form.save()
            usuario.save()
            return redirect('Menu:index',)
    else:
        form = Usuario_form()
        return render(request, 'index_add_us.html', {'form':form})

#Vista para los serializers
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializers


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializers