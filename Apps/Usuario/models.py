from django.db import models
from django.dispatch import receiver
from allauth.account.signals import user_signed_up

# Create your models here.
#Usuario
class Usuario(models.Model):
    rut = models.CharField(primary_key = True, max_length = 15)
    nombre = models.CharField(max_length = 40)
    telefono = models.CharField(max_length = 40)
    correo = models.EmailField()
    nacimiento = models.DateField()
    region = models.CharField(max_length = 30)
    comuna = models.CharField(max_length = 30)
    vivienda = models.CharField(max_length = 30)

    def Guardar(self):
        self.save()

    def __str__(self):
        return self.nombre


def create_user_profile(request, user, **kwargs):
    profile = Profile.objects.create(user=user)
    profile.save()