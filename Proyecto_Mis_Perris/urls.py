"""Proyecto_Mis_Perris URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.contrib.staticfiles.urls import static
from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView

from rest_framework import routers
from Apps.Usuario import views

from . import settings


router = routers.DefaultRouter()
router.register(r'user', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)


urlpatterns = [ 
    url(r'^admin/', admin.site.urls),
    url(r'', include('Apps.Menu.urls')),
    path('usuario', include(('Apps.Usuario.urls', 'usuario'), namespace='usuario')),
    path('accounts/', include('allauth.urls')),
    url(r'^mascota/', include ('Apps.Mascota.urls')),
    url(r'^usuario/', include ('Apps.Usuario.urls')),
    path('login/', LoginView.as_view(), name="Login"),
    path('logout/', LogoutView.as_view(), name="Logout"),
    path('misperrisApi/', include(router.urls)),
    path('', include('pwa.urls')), 
]

#nombre del admin
admin.site.site_header = "Administracion Mis Perris"
admin.site.index_title = "Mis Perris"
admin.site.site_title = "Administracion Mis Perris"

urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)