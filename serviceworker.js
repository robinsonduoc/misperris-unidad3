var CACHE_NAME = 'my-site-cache-v1';
var urlsToCache = [
    "/",
    '/static/css/style.css',
    
    '/static/img/articulos/perritoart1.png',
    '/static/img/articulos/perritoart2.png',
    '/static/img/icons/correo.png',
    '/static/img/icons/facebook.png',
    '/static/img/icons/google.png',
    '/static/img/icons/instagram.png',
    '/static/img/icons/patita-de-perro-1.png',
    '/static/img/rescatados/Bigotes.jpg',
    '/static/img/rescatados/Chocolate.jpg',
    '/static/img/rescatados/Luna.jpg',
    '/static/img/rescatados/Maya.jpg',
    '/static/img/rescatados/Oso.jpg',
    '/static/img/rescatados/Pexel.jpg',
    '/static/img/rescatados/Wifi.jpg',
    '/static/img/slide/Apolo.jpg',
    '/static/img/slide/Duque.jpg',
    '/static/img/slide/Tom.jpg',

    '/static/js/slide.js',



    'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js',
];


self.addEventListener('install', function(event) {
    // Perform install steps
    event.waitUntil(
      caches.open(CACHE_NAME)
        .then(function(cache) {
          console.log('Opened cache');
          return cache.addAll(urlsToCache);
        })
    );
  });
  
  self.addEventListener('fetch', function(event){
      event.respondWith(
          caches.match(event.request).then(function(response) {
              if(response) {
                  return response;
              }
  
              return fetch(event.request);
          })
      );
  });








